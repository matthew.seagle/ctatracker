function CTATracker_PopulateDungeonInfo()
    -- Collect some information about the player to determine elegibility.
    CTATracker_PlayerLevel = UnitLevel("player")
    CTATracker_PlayerItemLevel = {GetAverageItemLevel()} -- average, equipped    
    
    local tableIndex = 0 -- Used to increment indices in the array as items are added.
    -- Collect eligible dungeon information.
    for i = 1, GetNumRandomDungeons() do 
        local id,name = GetLFGRandomDungeonInfo(i)
        local sanitizedName = {string.gsub(name,"(%s+%p+.+%p+)",'')} -- Removes the (Expansion)
        local subtypeId = CTATracker_DungeonQueueable(id)
        if subtypeId > -1 then -- If -1 the player is not eligible for the dungeon so skip.
            tableIndex = tableIndex + 1 -- Going to add this dungeon to the eligibility list.
            CTATracker_AvailableDungeons[tableIndex] = CTATracker_AvailableInstanceInfo:new{
                dungeonId = id,
                dungeonSubtypeId = subtypeId,
                dungeonName = sanitizedName[1],
                tankShortage = false,
                healerShortage = false,
                dpsShortage = false
            }
        end
    end
    -- Collect eligible raid finder information.
    for i = 1, GetNumRFDungeons() do 
        local id,name = GetRFDungeonInfo(i)
        local sanitizedName = {string.gsub(name,"(%s+%p+.+%p+)",'')} -- Removes the (Expansion)
        local subtypeId = CTATracker_DungeonQueueable(id)
        if subtypeId > -1 then -- If 01 the player is not eligible for the LFR wing so skip.
            tableIndex = tableIndex + 1 -- Going to add this LFR wing to the eligibility list.
            CTATracker_AvailableDungeons[tableIndex] = CTATracker_AvailableInstanceInfo:new{
                dungeonId = id,
                dungeonSubtypeId = subtypeId,
                dungeonName = sanitizedName[1],
                tankShortage = false,
                healerShortage = false,
                dpsShortage = false
            }
        end
    end
    
    RequestLFDPlayerLockInfo(); -- Do initial role shortage check on load.
end

-- This determines using the players level and gear level if they are eligible for the dungeon/raid.
-- Returns either the dungeon subtypeId or -1 if the player is not eligible.
function CTATracker_DungeonQueueable(id) 
    -- 3,4,5,9,20
    local dungeonInfo = {GetLFGDungeonInfo(id)}
    local eligible = {GetLFGRoleShortageRewards(id,LFG_ROLE_SHORTAGE_RARE)} -- Need to check with the eligible flag as well since not all expansion instances return the ilvl info.
    
    if CTATracker_PlayerLevel >= dungeonInfo[4] and CTATracker_PlayerLevel <= dungeonInfo[5] and CTATracker_PlayerItemLevel[1] >= dungeonInfo[20] and eligible[1] then
        return dungeonInfo[3]
    end
    return -1
end

function CTATracker_RoleShortageCheck()
    CTATracker_NumberOfDungeonCTAs = 0
    -- print("Here looking for role shortages", #CTATracker_AvailableDungeons)
    if #CTATracker_AvailableDungeons == 0 then
        CTATracker_PopulateDungeonInfo() -- IF there are no dungeons in the list, see if you can repopulate it.
    end
    for i = 1, #CTATracker_AvailableDungeons do
        local _,tank,healer,dps = GetLFGRoleShortageRewards(CTATracker_AvailableDungeons[i].dungeonId,LFG_ROLE_SHORTAGE_RARE)
        CTATracker_AvailableDungeons[i].tankShortage = tank
        CTATracker_AvailableDungeons[i].healerShortage = healer
        CTATracker_AvailableDungeons[i].dpsShortage = dps

        if tank or healer or dps then
            CTATracker_NumberOfDungeonCTAs = CTATracker_NumberOfDungeonCTAs + 1
        end
    end
    -- Leaving in for easy debug
    -- print(GetTime(), "--------------------------------")
    -- for i = 1, #CTATracker_AvailableDungeons do        
    --     print(CTATracker_AvailableDungeons[i].dungeonId, 
    --     CTATracker_AvailableDungeons[i].dungeonSubtypeId,
    --     CTATracker_AvailableDungeons[i].dungeonName,
    --     CTATracker_AvailableDungeons[i].tankShortage,
    --     CTATracker_AvailableDungeons[i].healerShortage,
    --     CTATracker_AvailableDungeons[i].dpsShortage)
    -- end
    CTATracker_CreateDungeonFrames()
end

-- Routine timer for checking for role shortage changes.
function CTATracker_ConfigureTimer(self, elapsed)
    self.CTATracker_TimeSinceLastDungeonCheck = self.CTATracker_TimeSinceLastDungeonCheck + elapsed;
    if self.CTATracker_TimeSinceLastDungeonCheck >= 20.0 then
        RequestLFDPlayerLockInfo();
		self.CTATracker_TimeSinceLastDungeonCheck = 0.0
    end
end