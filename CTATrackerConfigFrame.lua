--------------------------------------------------------------------------------
--This class creates the base config frame.
--This is dynamically created within the addon
--------------------------------------------------------------------------------
function CTATracker_ConfigFrame()
    CTATracker_ConfigFrame = CreateFrame("Frame", "CTATracker_ConfigFrame", UIParent);
    CTATracker_ConfigFrame:SetPoint("CENTER", UIParent, 0, 0);
    CTATracker_ConfigFrame:SetMovable(true);
    CTATracker_ConfigFrame:EnableMouse(true);
    CTATracker_ConfigFrame:RegisterForDrag("LeftButton");
    CTATracker_ConfigFrame:SetScript("OnDragStart", CTATracker_ConfigFrame.StartMoving);
    CTATracker_ConfigFrame:SetScript("OnDragStop", CTATracker_ConfigFrame.StopMovingOrSizing);
    CTATracker_ConfigFrame:SetFrameStrata("BACKGROUND");
    CTATracker_ConfigFrame:SetWidth(220);
    CTATracker_ConfigFrame:SetHeight(340);
    CTATracker_ConfigFrame.texture = CTATracker_ConfigFrame:CreateTexture(nil, "BACKGROUND");
    CTATracker_ConfigFrame.texture:SetAllPoints(CTATracker_ConfigFrame);
    CTATracker_ConfigFrame.texture:SetTexture("Interface\\GLUES\\COMMON\\Glue-Tooltip-Background.blp", false);
    CTATracker_ConfigFrame:Hide();
  
    CTATracker_ConfigElements();
  end