function CTATracker_CreateBasicFrames()
  CTATracker_BasicFrameElements()
  CTATracker_CreateDungeonFrames()
  CTATracker_ConfigFrame()
end
--------------------------------------------------------------------------------
--Creates the dungeon frames
--------------------------------------------------------------------------------
--- collect data in array first
--- Pass data to 
function CTATracker_CreateDungeonFrames()
  if(CTATracker_Frames ~= nil) then
    CTATracker_CollectGarbage()
  end
  
  local height = 20
  local ctaCount = 1
  for i=1, #CTATracker_AvailableDungeons do
    if CTATracker_AvailableDungeons[i].tankShortage or CTATracker_AvailableDungeons[i].healerShortage or CTATracker_AvailableDungeons[i].dpsShortage then
      CTATracker_Frames[ctaCount] = CTATracker_DungeonFrame:new{
        dungeonFrameName = "CTATracker_DungeonFrame" .. ctaCount,
        buttonFrameName = "CTATracker_ButtonFrame" .. ctaCount,
        Frame = CreateFrame("Frame", dungeonFrameName, CTATracker_TopAnchorFrame),
        ButtonFrame = CreateFrame("Frame", buttonFrameName,nil),         
        xpos = 0,
        ypos = (34-(ctaCount*36)),
        frameWidth = 94
      }

      CTATracker_CurrentFrame(CTATracker_Frames[ctaCount].Frame, CTATracker_Frames[ctaCount].ButtonFrame, CTATracker_Frames[ctaCount].ypos, i);
      height = height + 36
      ctaCount = ctaCount + 1
    end
  end
  CTATracker_MainFrame:SetHeight(height);
end
--------------------------------------------------------------------------------
--Basic Template Frames
--------------------------------------------------------------------------------
function CTATracker_CurrentFrame(Frame, ButtonFrame, ypos, index)  
  Frame:SetPoint("TOP", CTATracker_TopAnchorFrame, 0, ypos);
  Frame:SetWidth(94);
  Frame:SetHeight(34);
  Frame.texture = Frame:CreateTexture(nil, "BACKGROUND");
  Frame.texture:SetAllPoints(Frame);
  Frame.texture:SetColorTexture(0.10,0.10,0.10,0.90);
  Frame:Show();

  FrameHeader = Frame:CreateFontString(nil, "ARTWORK", "GAMEFONTNORMAL");
  FrameHeader:SetFont("Fonts\\FRIZQT__.TTF", 8, "OUTLINE, MONOCHROME");
  FrameHeader:SetTextColor(1,1,1,1);
  FrameHeader:SetPoint("TOP", Frame, 0, -4);
  FrameHeader:SetText(CTATracker_AvailableDungeons[index].dungeonName);

  CTATracker_ButtonTemplateFrame(Frame, ButtonFrame, index);
end
--------------------------------------------------------------------------------
--Basic Template Button Frame
--------------------------------------------------------------------------------
function CTATracker_ButtonTemplateFrame(Frame, ButtonFrame, index)
  ButtonFrame:SetParent(Frame);
  ButtonFrame:SetPoint("TOP", Frame, 0, -15);
  ButtonFrame:SetWidth(90);
  ButtonFrame:SetHeight(18);
  ButtonFrame.texture = ButtonFrame:CreateTexture(nil, "BACKGROUND");
  ButtonFrame.texture:SetAllPoints(ButtonFrame);
  ButtonFrame.texture:SetColorTexture(0,0,0,1);
  ButtonFrame:Show();

  CTATracker_TankButtonTemplate(ButtonFrame, index);
  CTATracker_HealerButtonTemplate(ButtonFrame, index);
  CTATracker_DPSButtonTemplate(ButtonFrame, index);
end
--------------------------------------------------------------------------------
--Basic Tank Button
--------------------------------------------------------------------------------
function CTATracker_TankButtonTemplate(ButtonFrame, index)
  local tankButton = CreateFrame("Button","tankButton",ButtonFrame)
  tankButton:EnableMouse(true)
  tankButton:SetPoint("LEFT",ButtonFrame,0,0)
  tankButton:SetWidth(16)
  tankButton:SetHeight(16)
  tankButton:SetFrameStrata("MEDIUM")

  local textureTank = tankButton:CreateTexture()
  textureTank:SetTexture("Interface\\LFGFRAME\\UI-LFG-ICON-ROLES.blp")
  textureTank:SetTexCoord(0,0.26,0.26,0.52)
  textureTank:SetAllPoints()
  tankButton:SetNormalTexture(textureTank)

  local htex = tankButton:CreateTexture()
  htex:SetTexture("Interface/Buttons/UI-Panel-Button-Highlight")
  htex:SetTexCoord(0,0.625,0,0.6875)
  htex:SetAllPoints()
  tankButton:SetHighlightTexture(htex)

  local ptex = tankButton:CreateTexture()
  ptex:SetTexture("Interface\\LFGFrame\\UI-LFG-ICON-ROLES.blp")
  ptex:SetTexCoord(0,0.26,0.26,0.52)
  ptex:SetAllPoints()
  tankButton:SetPushedTexture(ptex)
  tankButton:SetScript("OnClick", function()
    CTATracker_LFGRoles["tank"] = not(CTATracker_LFGRoles["tank"]) 
    SetLFGRoles(CTATracker_LFGRoles["leader"],CTATracker_LFGRoles["tank"],CTATracker_LFGRoles["healer"],CTATracker_LFGRoles["dps"]);
    print(CTATracker_AvailableDungeons[index].dungeonId, CTATracker_AvailableDungeons[index].dungeonSubtypeId, CTATracker_AvailableDungeons[index].dungeonName, CTATracker_AvailableDungeons[index].tankShortage);
   end)

  local RoleSelectedFrame = CreateFrame("Frame", "CTATracker_TankRoleSelectedFrame" .. index,tankButton)
  RoleSelectedFrame:SetPoint("CENTER",tankButton:GetWidth()/2,-tankButton:GetHeight()/2.3)
  RoleSelectedFrame:SetWidth(tankButton:GetWidth()/1.5)
  RoleSelectedFrame:SetHeight(tankButton:GetHeight()/1.5)
  RoleSelectedFrame:SetFrameStrata("MEDIUM")
  RoleSelectedFrame.texture = RoleSelectedFrame:CreateTexture(nil, "BACKGROUND")
  RoleSelectedFrame.texture:SetTexture("Interface\\RAIDFRAME\\ReadyCheck-Ready.blp")
  RoleSelectedFrame.texture:SetAllPoints(RoleSelectedFrame)
  RoleSelectedFrame.texture:SetTexCoord(0,1,0,1)
  RoleSelectedFrame:Show()

  if CTATracker_AvailableDungeons[index].tankShortage then
    tankButton:Show()
  else
    tankButton:Hide()
  end
end
--------------------------------------------------------------------------------
--Basic Healer Button
--------------------------------------------------------------------------------
function CTATracker_HealerButtonTemplate(ButtonFrame, index)
  local healerButton = CreateFrame("Button","healerButton",ButtonFrame);
  healerButton:EnableMouse(true);
  healerButton:SetPoint("CENTER",ButtonFrame,0,0);
  healerButton:SetWidth(16);
  healerButton:SetHeight(16);
  healerButton:SetFrameStrata("MEDIUM");

  local textureHealer = healerButton:CreateTexture();
  textureHealer:SetTexture("Interface\\LFGFRAME\\UI-LFG-ICON-ROLES.blp");
  textureHealer:SetTexCoord(0.26,0.52,0,0.26);
  textureHealer:SetAllPoints(healerButton);
  healerButton:SetNormalTexture(textureHealer);

  local htex = healerButton:CreateTexture();
  htex:SetTexture("Interface/Buttons/UI-Panel-Button-Highlight");
  htex:SetTexCoord(0,0.625,0,0.6875);
  htex:SetAllPoints();
  healerButton:SetHighlightTexture(htex);

  local ptex = healerButton:CreateTexture();
  ptex:SetTexture("Interface\\LFGFRAME\\UI-LFG-ICON-ROLES.blp");
  ptex:SetTexCoord(0.26,0.52,0,0.26);
  ptex:SetAllPoints();
  healerButton:SetPushedTexture(ptex);
  healerButton:SetScript("OnClick", function()
    CTATracker_LFGRoles["healer"] = not(CTATracker_LFGRoles["healer"])
    SetLFGRoles(CTATracker_LFGRoles["leader"],CTATracker_LFGRoles["tank"],CTATracker_LFGRoles["healer"],CTATracker_LFGRoles["dps"]);
    print(CTATracker_AvailableDungeons[index].dungeonId, CTATracker_AvailableDungeons[index].dungeonSubtypeId, CTATracker_AvailableDungeons[index].dungeonName, CTATracker_AvailableDungeons[index].healerShortage);
   end)

  local RoleSelectedFrame = CreateFrame("Frame", "CTATracker_HealerRoleSelectedFrame" .. index,healerButton)
  RoleSelectedFrame:SetPoint("CENTER",healerButton:GetWidth()/2,-healerButton:GetHeight()/2.3)
  RoleSelectedFrame:SetWidth(healerButton:GetWidth()/1.5)
  RoleSelectedFrame:SetHeight(healerButton:GetHeight()/1.5)
  RoleSelectedFrame:SetFrameStrata("MEDIUM")
  RoleSelectedFrame.texture = RoleSelectedFrame:CreateTexture(nil, "BACKGROUND")
  RoleSelectedFrame.texture:SetTexture("Interface\\RAIDFRAME\\ReadyCheck-Ready.blp")
  RoleSelectedFrame.texture:SetAllPoints(RoleSelectedFrame)
  RoleSelectedFrame.texture:SetTexCoord(0,1,0,1)
  RoleSelectedFrame:Show()
  
  if CTATracker_AvailableDungeons[index].healerShortage then
    healerButton:Show()
  else
    healerButton:Hide()
  end
end

--------------------------------------------------------------------------------
--Basic DPS Button
--------------------------------------------------------------------------------
function CTATracker_DPSButtonTemplate(ButtonFrame, index)
  local dpsButton = CreateFrame("Button","dpsButton",ButtonFrame);
  dpsButton:SetPoint("RIGHT", ButtonFrame,0,0);
  dpsButton:EnableMouse(true);
  dpsButton:SetWidth(16);
  dpsButton:SetHeight(16);
  dpsButton:SetFrameStrata("MEDIUM");

  local textureDPS = dpsButton:CreateTexture();
  textureDPS:SetTexture("Interface\\LFGFrame\\UI-LFG-ICON-ROLES.blp");
  textureDPS:SetTexCoord(0.26,0.52,0.26,0.52);
  textureDPS:SetAllPoints(dpsButton);
  dpsButton:SetNormalTexture(textureDPS);

  local htex = dpsButton:CreateTexture();
  htex:SetTexture("Interface/Buttons/UI-Panel-Button-Highlight");
  htex:SetTexCoord(0,0.625,0,0.6875);
  htex:SetAllPoints();
  dpsButton:SetHighlightTexture(htex);

  local ptex = dpsButton:CreateTexture();
  ptex:SetTexture("Interface\\LFGFrame\\UI-LFG-ICON-ROLES.blp");
  ptex:SetTexCoord(0.26,0.52,0.26,0.52);
  ptex:SetAllPoints();
  dpsButton:SetPushedTexture(ptex);
  dpsButton:SetScript("OnClick", function()
    CTATracker_LFGRoles["dps"] = not(CTATracker_LFGRoles["dps"])
    SetLFGRoles(CTATracker_LFGRoles["leader"],CTATracker_LFGRoles["tank"],CTATracker_LFGRoles["healer"],CTATracker_LFGRoles["dps"]);
    print(CTATracker_AvailableDungeons[index].dungeonId, CTATracker_AvailableDungeons[index].dungeonSubtypeId, CTATracker_AvailableDungeons[index].dungeonName, CTATracker_AvailableDungeons[index].dpsShortage);
  end)

  local RoleSelectedFrame = CreateFrame("Frame", "CTATracker_DPSRoleSelectedFrame" .. index,dpsButton)
  RoleSelectedFrame:SetPoint("CENTER",dpsButton:GetWidth()/2,-dpsButton:GetHeight()/2.3)
  RoleSelectedFrame:SetWidth(dpsButton:GetWidth()/1.5)
  RoleSelectedFrame:SetHeight(dpsButton:GetHeight()/1.5)
  RoleSelectedFrame:SetFrameStrata("MEDIUM")
  RoleSelectedFrame.texture = RoleSelectedFrame:CreateTexture(nil, "BACKGROUND")
  RoleSelectedFrame.texture:SetTexture("Interface\\RAIDFRAME\\ReadyCheck-Ready.blp")
  RoleSelectedFrame.texture:SetAllPoints(RoleSelectedFrame)
  RoleSelectedFrame.texture:SetTexCoord(0,1,0,1)
  RoleSelectedFrame:Show()

  if CTATracker_AvailableDungeons[index].dpsShortage then
    dpsButton:Show()
  else
    dpsButton:Hide()
  end
end

function CTATracker_BasicFrameElements()
  ------------------------------------------------------------------------------
  --Close Tracker Button
  ------------------------------------------------------------------------------
  local CTATracker_CloseTracker = CreateFrame("Button", nil, CTATracker_BottomAnchorFrame);
  CTATracker_CloseTracker:SetPoint("TOP", CTATracker_BottomAnchorFrame, 44, 12);
  CTATracker_CloseTracker:SetWidth(10);
  CTATracker_CloseTracker:SetHeight(10);

  CTATracker_CloseTracker:SetText("X");
  CTATracker_CloseTracker:SetNormalFontObject("GameFontNormal");

  --Close Button Texture Status: Normal, Highlighted, Pushed
  --Normal
  local ntex = CTATracker_CloseTracker:CreateTexture()
  ntex:SetTexture("Interface/Buttons/UI-Panel-Button-Up");
  ntex:SetTexCoord(0, 0.625, 0, 6875);
  ntex:SetAllPoints();
  CTATracker_CloseTracker:SetNormalTexture(ntex);
  --Highlighted
  local htex = CTATracker_CloseTracker:CreateTexture();
  htex:SetTexture("Interface/Buttons/UI-AttributeButton-Encourage-Hilight");
  htex:SetTexCoord(0, 1, 0, 1);
  htex:SetAllPoints();
  CTATracker_CloseTracker:SetHighlightTexture(htex);
  --Pushed
  local ptex = CTATracker_CloseTracker:CreateTexture();
  ptex:SetTexture("Interface/Buttons/UI-Panel-Button-Down");
  ptex:SetTexCoord(0, 0.625, 0, 0.6875);
  ptex:SetAllPoints();
  CTATracker_CloseTracker:SetPushedTexture(ptex);
  CTATracker_CloseTracker:SetScript("OnClick", function() CTATracker_MainFrame:Hide(); end);
  ------------------------------------------------------------------------------
  --Config Button
  ------------------------------------------------------------------------------
  local CTATracker_ConfigButton = CreateFrame("Button", nil, CTATracker_BottomAnchorFrame);
  CTATracker_ConfigButton:SetPoint("TOP", CTATracker_BottomAnchorFrame, 30, 15);
  CTATracker_ConfigButton:SetWidth(15);
  CTATracker_ConfigButton:SetHeight(15);

  CTATracker_ConfigButton:SetText("");
  CTATracker_ConfigButton:SetNormalFontObject("GameFontNormal");
  --Normal
  local ntex = CTATracker_ConfigButton:CreateTexture();
  ntex:SetTexture("Interface/Buttons/UI-OptionsButton");
  ntex:SetTexCoord(0,1,0,1);
  --ntex:SetTexCoord(0, 0.625, 0, 0.6875)
  ntex:SetAllPoints();
  CTATracker_ConfigButton:SetNormalTexture(ntex);
  --Highlighted
  local htex = CTATracker_ConfigButton:CreateTexture();
  htex:SetTexture("Interface/Buttons/UI-AttributeButton-Encourage-Hilight");
  htex:SetTexCoord(0, 1, 0, 1);
  htex:SetAllPoints();
  CTATracker_ConfigButton:SetHighlightTexture(htex);
  --Pushed
  local ptex = CTATracker_ConfigButton:CreateTexture();
  ptex:SetTexture("Interface/Buttons/UI-OptionsButton");
  ptex:SetTexCoord(0, 1, 0, 1);
  ptex:SetAllPoints();
  CTATracker_ConfigButton:SetPushedTexture(ptex);
  CTATracker_ConfigButton:SetScript("OnClick", function() CTATracker_ConfigFrame:Show(); end);
end

--------------------------------------------------------------------------------
-- Cleanup old frames.
--------------------------------------------------------------------------------
function CTATracker_CollectGarbage()
  if CTATracker_Frames ~= nil then
    for i=1, #CTATracker_Frames do
      CTATracker_Frames[i].Frame:Hide()
      CTATracker_Frames[i] = nil;
    end
    collectgarbage("collect")
  end
end
