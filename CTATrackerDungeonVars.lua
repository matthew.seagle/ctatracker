CTATracker_AvailableDungeons = {}
CTATracker_NumberOfDungeonCTAs = 0
CTATracker_LFGRoles = setmetatable({leader = false, tank = false, healer = false, dps = false}, {
    __newindex = function(CTATracker_LFGRoles, key, value)
       rawset(CTATracker_LFGRoles, key, value)
    end
 })


CTATracker_AvailableInstanceInfo = {
    dungeonId = -1,
    dungeonSubtypeId = 0,
    dungeonName = "",
    tankShortage = false,
    healerShortage = false,
    dpsShortage = false,

    setId = function(self, dungeonId)
        self.dungeonId = dungeonId
    end,
    setDungeonSubtypeId = function(self, dungeonSubtypeId)
        self.dungeonSubtypeId = dungeonSubtypeId
    end,
    setName = function(self, dungeonName)
        self.dungeonName = dungeonName
    end,
    setTankShortage = function(self, tankShortage)
        self.tankShortage = tankShortage
    end,
    setHealerShortage = function(self, healerShortage)
        self.healerShortage = healerShortage
    end,
    setDpsShortage = function(self, dpsShortage)
        self.dpsShortage = dpsShortage
    end,
    new = function(self, o)
        o = o or {}
        setmetatable(o, self)
        self.__index = self
        return o
    end
}

CTATracker_PlayerLevel = 1
CTATracker_PlayerItemLevel = nil