## Interface: 80000
## Title: CTA Tracker
## Author: Matthew Seagle
## Notes: Track Call To Arms
## SavedVariablesPerCharacter: CTATracker_ScaleWatch
CTATrackerUiVars.lua
CTATrackerDungeonVars.lua
CTATrackerDungeonEngine.lua
CTATrackerFrameClass.lua
CTATrackerSC.lua
CTATrackerBasicFrames.lua
CTATrackerConfigFrame.lua
CTATrackerConfigElements.lua
CTATracker.xml
